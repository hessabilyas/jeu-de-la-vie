### Importations
from random import randint
from time import sleep

### Classes
class Cellule():
    def __init__ (self, actuel = False, futur = False, voisins = None) -> None:
        self._actuel = actuel
        self._futur = futur
        self._voisins = voisins

    def est_vivant(self):
        return self._actuel

    def set_voisins(self, list) -> None:
        self._voisins = list

    def get_voisins(self):
        return self._voisins
    
    def naître(self):
        self._futur = True
    
    def mourir(self):
        self._futur = False
    
    def basculer(self):
        self._actuel = self._futur

    def __str__(self):
        if self._actuel == False:
            return '-'
        else:
            return 'X'
    
    def calcule_etat_futur(self):
        if len(self._voisins) > 3 or len(self._voisins) < 2:
            self.mourir()
        elif self._actuel == False and len(self._voisins) == 2:
            pass
        else:
            self.naître()

class Grille():
    def __init__(self, largeur, hauteur, matrix = None):
        self.largeur = largeur
        self.hauteur = hauteur
        self.matrix = [[Cellule() for _ in range(self.largeur)] for _ in range(self.hauteur)]
    
    def dans_grille(self, i, j):
        if i >= 0 and i <= self.largeur and j >= 0 and j <= self.hauteur:
            return True
        else:
            return False
    
    def setXY(self, i, j, val):
        self.matrix[i, j]
        val = Cellule()

    def getXY(self):
        return self.matrix[i,j]

    def est_voisin(self, i, j, x, y):
        if x - 1 <= i and x + 1 >= i:
            if y - 1 <= j and y + 1 >= j:
                return True
            else:
                return False
        else:
            return False

    def get_voisins(self, i, j):
        voisins = []
        for y in range(self.largeur):
            for x in range(self.hauteur):
                if i == x and j == y:
                    pass
                elif self.est_voisin(i,j,x,y) and self.matrix[x][y].est_vivant():
                    voisins.append([x,y])
        return voisins

    def affecte_voisins(self):
        for y in range(self.largeur):
            for x in range(self.hauteur):
                self.matrix[x][y].set_voisins(self.get_voisins(x, y))


    def __str__(self):
        for k in self.matrix:
            R = ""
            for i in k:
                R = R + str(i)
            print(R)
        return ""

    def remplir_alea(self):
        for y in range(self.largeur):
            for x in range(self.hauteur):
                m = randint(0,1)
                if m == 1:
                    self.matrix[x][y].naître()
                    self.matrix[x][y].basculer()
    
    def jeu(self):
        self.affecte_voisins()
        for y in range(self.largeur):
            for x in range(self.hauteur):
                self.matrix[x][y].calcule_etat_futur()

    def actualise(self):
        for y in range(self.largeur):
            for x in range(self.hauteur):
                self.matrix[x][y].basculer()

### Programme Principal

val = input("Combien de générations voulez-vous créer ?")
p = Grille(50,25)
p.remplir_alea()
for _ in range(int(val)):
    p.jeu()
    p.actualise()
    sleep(1)
    print(p)